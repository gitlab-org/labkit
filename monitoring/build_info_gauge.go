package monitoring

import "github.com/prometheus/client_golang/prometheus"

const (
	// GitlabBuildInfoGaugeMetricName is the name of the label containing
	// build information for this process.
	GitlabBuildInfoGaugeMetricName = "gitlab_build_info"

	buildInfoVersionLabel       = "version"
	buildInfoBuildTimeLabel     = "built"
	buildInfoModifiedLabel      = "modified"
	buildInfoCommittedLabel     = "committed"
	buildInfoGoVersionLabel     = "go_version"
	buildInfoModulePathLabel    = "module_path"
	buildInfoModuleVersionLabel = "module_version"
)

// registerBuildInfoGauge registers a label with the current server version
// making it easy to see what versions of the application are running across a cluster.
func registerBuildInfoGauge(registerer prometheus.Registerer, labels prometheus.Labels) {
	gitlabBuildInfoGauge := prometheus.NewGauge(prometheus.GaugeOpts{
		Name:        GitlabBuildInfoGaugeMetricName,
		Help:        "Current build info for this GitLab Service",
		ConstLabels: labels,
	})
	registerer.MustRegister(gitlabBuildInfoGauge)
	gitlabBuildInfoGauge.Set(1)
}
