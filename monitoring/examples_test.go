package monitoring_test

import (
	"log"
	"runtime/debug"

	"gitlab.com/gitlab-org/labkit/monitoring"
)

func ExampleStart() {
	go func() {
		// Add the standard version and build time labels
		buildInfoOption := monitoring.WithBuildInformation("0.1.1", "2019-09-01T00:22:00Z")
		if buildInfo, ok := debug.ReadBuildInfo(); ok {
			// If we're able to read the Go build information from
			// the currently running binary then we use that as
			// build info option instead. Binaries that are built
			// from a Go project with module support should always
			// have build info available.
			buildInfoOption = monitoring.WithGoBuildInformation(buildInfo)
		}

		log.Fatal(monitoring.Start(
			// Listen on port 7822 on all interfaces
			monitoring.WithListenerAddress(":7822"),
			// Add the standard version and build time labels
			buildInfoOption,
			// Add any additional application-specific labels to the `gitlab_build_info` metric
			monitoring.WithBuildExtraLabels(map[string]string{
				"git_version": "2.0.0",
			}),
		))
	}()
}
