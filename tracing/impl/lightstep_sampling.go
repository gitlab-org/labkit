//go:build tracer_static && tracer_static_lightstep
// +build tracer_static,tracer_static_lightstep

package impl

import (
	"github.com/lightstep/lightstep-tracer-go"
	"github.com/opentracing/opentracing-go"
)

func init() {
	is := IsSampled
	IsSampled = func(span opentracing.Span) bool {
		spanContext := span.Context()

		if lightstepContext, ok := spanContext.(lightstep.SpanContext); ok {
			return lightstepContext.Sampled == "true"
		}
		return is(span)
	}
}
