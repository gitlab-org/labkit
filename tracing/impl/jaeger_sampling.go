//go:build tracer_static && tracer_static_jaeger
// +build tracer_static,tracer_static_jaeger

package impl

import (
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
)

func init() {
	is := IsSampled
	IsSampled = func(span opentracing.Span) bool {
		spanContext := span.Context()

		if jaegerContext, ok := spanContext.(jaeger.SpanContext); ok {
			return jaegerContext.IsSampled()
		}
		return is(span)
	}
}
