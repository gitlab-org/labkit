#!/usr/bin/env sh

set -eux

IFS="$(printf '\n\t')"

dir=$(cd -- "$(dirname -- "$0")" && pwd)

cd "${dir}"

for build_tags in \
  "" \
  "tracer_static tracer_static_jaeger tracer_static_lightstep tracer_static_datadog tracer_static_stackdriver" \
  "continuous_profiler_stackdriver" \
  ; do
  (

    go test \
    -race \
    -tags "${build_tags} ${REQUIRED_BUILD_TAGS:-}" \
    ./...
  )
done
