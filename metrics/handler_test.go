package metrics

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/prometheus/client_golang/prometheus"
	dto "github.com/prometheus/client_model/go"
	"github.com/stretchr/testify/require"
)

func TestNewHandlerFactory(t *testing.T) {
	var dynamicLabelKey struct{}

	tests := []struct {
		name        string
		opts        []HandlerFactoryOption              // factory-level options
		handlerOpts []HandlerOption                     // handler-level options
		injectCtx   func(r *http.Request) *http.Request // optional context injection
		wantLabels  map[string]string                   // custom label key/values
	}{
		{
			name: "basic",
			opts: []HandlerFactoryOption{
				WithNamespace("basic"),
			},
		},
		{
			name: "labels",
			opts: []HandlerFactoryOption{
				WithNamespace("labels"),
				WithLabels("label1", "label2"),
			},
			handlerOpts: []HandlerOption{
				WithLabelValues(map[string]string{"label1": "1", "label2": "1"}),
			},
			wantLabels: map[string]string{"label1": "1", "label2": "1"},
		},
		{
			name: "dynamic_labels",
			opts: []HandlerFactoryOption{
				WithNamespace("dynamic_labels"),
				WithLabels("color"),
			},
			handlerOpts: []HandlerOption{
				WithLabelFromContext("color", func(ctx context.Context) string {
					if c, ok := ctx.Value(dynamicLabelKey).(string); ok {
						return c
					}
					return "unknown"
				}),
			},
			injectCtx: func(r *http.Request) *http.Request {
				ctx := context.WithValue(r.Context(), dynamicLabelKey, "green")
				return r.WithContext(ctx)
			},
			wantLabels: map[string]string{"color": "green"},
		},
		{
			name: "request_duration_buckets",
			opts: []HandlerFactoryOption{
				WithNamespace("request_duration_buckets"),
				WithRequestDurationBuckets([]float64{1, 2, 3}),
			},
		},
		{
			name: "time_to_write_header_duration_buckets",
			opts: []HandlerFactoryOption{
				WithNamespace("time_to_write_header_duration_buckets"),
				WithTimeToWriteHeaderDurationBuckets([]float64{1, 2, 3}),
			},
		},
		{
			name: "byte_bucket_size_buckets",
			opts: []HandlerFactoryOption{
				WithNamespace("byte_bucket_size_buckets"),
				WithByteSizeBuckets([]float64{1, 2, 3}),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			factory := NewHandlerFactory(tt.opts...)
			require.NotNil(t, factory)

			var invoked bool
			handler := factory(
				http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					invoked = true
					w.WriteHeader(200)
					fmt.Fprint(w, "OK")
				}),
				tt.handlerOpts...,
			)

			r := httptest.NewRequest("GET", "http://example.com", nil)
			if tt.injectCtx != nil {
				r = tt.injectCtx(r)
			}

			// Serve the request
			w := httptest.NewRecorder()
			handler.ServeHTTP(w, r)
			require.True(t, invoked, "handler was never invoked")

			// Gather metrics from the default registry
			metrics, err := prometheus.DefaultGatherer.Gather()
			require.NoError(t, err)
			require.NotEmpty(t, metrics)

			keyedMetrics := make(map[string]*dto.MetricFamily, len(metrics))
			for _, v := range metrics {
				keyedMetrics[*v.Name] = v
			}

			// For each test, the namespace is "tt.name", so we expect e.g. "basic_http_requests_total"
			ns := tt.name + "_http_"

			// Since each test calls 1 request, we expect:
			//  - in-flight gauge => 0 after finishing
			//  - counters/histograms => sample count 1
			checkGauge(t, keyedMetrics, ns+httpInFlightRequestsMetricName, 0, nil)
			checkCounter(t, keyedMetrics, ns+httpRequestsTotalMetricName, 1, tt.wantLabels)
			checkHistogramCount(t, keyedMetrics, ns+httpRequestDurationSecondsMetricName, 1, tt.wantLabels)
			checkHistogramCount(t, keyedMetrics, ns+httpRequestSizeBytesMetricName, 1, tt.wantLabels)
			checkHistogramCount(t, keyedMetrics, ns+httpResponseSizeBytesMetricName, 1, tt.wantLabels)
			checkHistogramCount(t, keyedMetrics, ns+httpTimeToWriteHeaderSecondsMetricName, 1, tt.wantLabels)
		})
	}
}

func checkGauge(
	t *testing.T,
	keyed map[string]*dto.MetricFamily,
	name string,
	wantVal float64,
	wantLabels map[string]string,
) {
	t.Helper()

	metric := getMetric(t, keyed, name, wantLabels)
	g := metric.GetGauge()
	require.NotNilf(t, g, "expected gauge metric for %q", name)
	require.Equal(t, wantVal, g.GetValue())
}

func checkCounter(
	t *testing.T,
	keyed map[string]*dto.MetricFamily,
	name string,
	wantVal float64,
	wantLabels map[string]string,
) {
	t.Helper()

	metric := getMetric(t, keyed, name, wantLabels)
	c := metric.GetCounter()
	require.NotNilf(t, c, "expected counter metric for %q", name)
	require.Equal(t, wantVal, c.GetValue())
}

func checkHistogramCount(
	t *testing.T,
	keyed map[string]*dto.MetricFamily,
	name string,
	wantCount uint64,
	wantLabels map[string]string,
) {
	t.Helper()

	metric := getMetric(t, keyed, name, wantLabels)
	h := metric.GetHistogram()
	require.NotNilf(t, h, "expected histogram metric for %q", name)
	require.Equal(t, wantCount, h.GetSampleCount())
}

// getMetric looks up the MetricFamily by name, finds the sample
// matching wantLabels, and returns that single *dto.Metric for further checks.
func getMetric(
	t *testing.T,
	keyed map[string]*dto.MetricFamily,
	name string,
	wantLabels map[string]string,
) *dto.Metric {
	t.Helper()

	mf := keyed[name]
	require.NotNilf(t, mf, "no metric named %q", name)

	m := findMetricWithLabels(t, mf.Metric, wantLabels)
	require.NotNilf(t, m, "no metric matching labels %v for %q", wantLabels, name)

	return m
}

// findMetricWithLabels returns the first metric whose label set matches
// all of wantLabels. If wantLabels is empty, returns the first metric.
func findMetricWithLabels(
	t *testing.T,
	metrics []*dto.Metric,
	wantLabels map[string]string,
) *dto.Metric {
	t.Helper()

	if len(wantLabels) == 0 {
		// No label constraints => return the first if available
		if len(metrics) > 0 {
			return metrics[0]
		}
		return nil
	}

	for _, m := range metrics {
		if hasAllLabels(m.Label, wantLabels) {
			return m
		}
	}
	return nil
}

// hasAllLabels checks whether labelPairs contain all key=val pairs in wantLabels.
func hasAllLabels(labelPairs []*dto.LabelPair, wantLabels map[string]string) bool {
	for k, v := range wantLabels {
		match := false
		for _, lp := range labelPairs {
			if lp.GetName() == k && lp.GetValue() == v {
				match = true
				break
			}
		}
		if !match {
			return false
		}
	}
	return true
}
