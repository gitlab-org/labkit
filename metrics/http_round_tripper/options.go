package http_round_tripper

type Config struct {
	labelValues map[string]string
}

// Option is used to pass options to the Factory instance.
type Option func(*Config)

func applyOptions(opts []Option) Config {
	config := Config{}
	for _, v := range opts {
		v(&config)
	}

	return config
}

// WithLabelValues will configure labels values to apply to this round tripper.
func WithLabelValues(labelValues map[string]string) Option {
	return func(config *Config) {
		config.labelValues = labelValues
	}
}
