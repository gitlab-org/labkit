package metrics_test

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"net/http"

	"gitlab.com/gitlab-org/labkit/metrics"
)

func ExampleNewHandlerFactory() {
	// Tell prometheus to include a "route" label for the http metrics
	newMetricHandlerFunc := metrics.NewHandlerFactory(metrics.WithLabels("route"))

	http.Handle("/foo",
		newMetricHandlerFunc(
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintf(w, "Hello foo")
			}),
			metrics.WithLabelValues(map[string]string{"route": "/foo"}), // Add instrumentation with a custom label of route="/foo"
		))

	http.Handle("/bar",
		newMetricHandlerFunc(
			http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				fmt.Fprintf(w, "Hello bar")
			}),
			metrics.WithLabelValues(map[string]string{"route": "/bar"}), // Add instrumentation with a custom label of route="/bar"
		))

	log.Fatal(http.ListenAndServe(":8080", nil))
}

// ExampleWithLabelFromContext shows how metrics can be configured to use dynamic labels pulled from the
// request context.
func ExampleWithLabelFromContext() {
	// We'll store a random color in the request context using a custom key.
	type ctxKeyType struct{}
	var colorKey = ctxKeyType{}

	// A simple middleware that assigns a random color and puts it in the request context.
	injectColor := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			color := "blue"
			if rand.Intn(2) == 0 {
				color = "red"
			}
			ctx := context.WithValue(r.Context(), colorKey, color)

			log.Printf("Injected color: %s", color)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}

	// Create a metrics handler factory with a custom "color" label
	newMetricHandlerFunc := metrics.NewHandlerFactory(
		metrics.WithLabels("color"),
	)

	// Define final handler
	finalHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello dynamic color!\n")
	})

	// Wrap final handler with LabKit instrumentation, telling it how to extract
	// "color" from the request context.
	instrumentedHandler := newMetricHandlerFunc(
		finalHandler,
		metrics.WithLabelFromContext("color", func(ctx context.Context) string {
			if c, ok := ctx.Value(colorKey).(string); ok {
				return c
			}
			return "unknown"
		}),
	)

	// Wrap the instrumented handler with injectColor
	http.Handle("/color", injectColor(instrumentedHandler))

	log.Println("Starting server on :8080 for label from context example...")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
