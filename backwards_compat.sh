#!/usr/bin/env sh

set -eux

[ -z "${1:-}" ] && (
	echo "INFO: Please specify a HTTPS git URL to process."
	exit 1
)

repo=${1}
dir=${2:-}

cloneDir=$(mktemp -d)

CI_PROJECT_DIR=${CI_PROJECT_DIR:-$(pwd)}

git clone --depth 1 "$repo" "$cloneDir"
cd "$cloneDir/$dir"

grep -E '^go ' go.mod

go mod edit -replace=gitlab.com/gitlab-org/labkit="$CI_PROJECT_DIR"

# Ensure go.mod and go.sum are up to date in the cloned repo, otherwise build may fail.
go mod tidy

make -j "$(nproc)"

rm -rf "$cloneDir"
